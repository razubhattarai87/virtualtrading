from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from todaysprice.models import StockAndNumbers,Brokers,ListedCompanies
from todaysprice.serializers import BrokerSerializers,ListedCompanySerializers
from rest_framework.permissions import IsAuthenticated,AllowAny
from todaysprice.scrapping import getnews_bizmandu,get_nepalipaisa_news,get_sharesansar_news,get_merolagani_news,get_all_listedcomapnies,get_all_brokers,top_sharetraded,top_transcations,top_turnover,get_chart_company_data,company_details,getStocksAndNumbers,scrape_todaysprice_sharesansar,scrape,floorsheet,market_subindex,market_information,market_summary,sectorwise_summary,top_loosers,top_gainers
import json
# Create your views here.

class TodaysPrice(APIView):
    permission_classes=(IsAuthenticated,)
    def get(self,request,format=None):
        #print(scrape())
        #print(json.loads(json.dumps(scrape())))
        #getStocksAndNumbers()
        #get_chart_data("131","D")
        #get_all_brokers()
        get_all_listedcomapnies()
        return Response({'msg':'Success','results':json.loads(json.dumps(get_all_listedcomapnies()))},status=status.HTTP_200_OK)

class SectorSummary(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'Success','results':json.loads(json.dumps(sectorwise_summary()))},status=status.HTTP_200_OK)

class TopLoosers(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'Success','results':json.loads(json.dumps(top_loosers()))},status=status.HTTP_200_OK)

class TopGainers(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'Success','results':json.loads(json.dumps(top_gainers()))},status=status.HTTP_200_OK)
    
class MarketSummary(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'Success','results':json.loads(json.dumps(market_summary()))},status=status.HTTP_200_OK)

class MarketInformation(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'Success','results':json.loads(json.dumps(market_information()))},status=status.HTTP_200_OK)

class MarketSubindex(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'Success','indices':json.loads(json.dumps(market_information())),'subindices':json.loads(json.dumps(market_subindex()))},status=status.HTTP_200_OK)

class Floorsheet(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'Success','floorsheet_company':json.loads(json.dumps(floorsheet()))},status=status.HTTP_200_OK)

class GetDetailsOfCompany(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,symbol,format=None):
        num = StockAndNumbers.objects.get(symbol=symbol)
        return Response({'msg':'success','chartdata':get_chart_company_data(num.number,'D'),'companydetails':json.loads(json.dumps(company_details(num.number)))},status=status.HTTP_200_OK)

class GetTopTurnover(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'success','top_turnover':json.loads(json.dumps(top_turnover()))},status=status.HTTP_200_OK)

class GetTopShareTraded(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'success','top_sharetraded':json.loads(json.dumps(top_sharetraded()))},status=status.HTTP_200_OK)

class GetTopTranscations(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        return Response({'msg':'success','top_transcations':json.loads(json.dumps(top_transcations()))},status=status.HTTP_200_OK)

class GetAllBrokers(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        brokers = Brokers.objects.all()
        serializers = BrokerSerializers(brokers,many=True)
        return Response({'msg':'success','brokers':serializers.data},status=status.HTTP_200_OK)
        

class GetAllCompany(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        companies = ListedCompanies.objects.all()
        serializers = ListedCompanySerializers(companies,many=True)
        return Response({'msg':'success','companies':serializers.data},status=status.HTTP_200_OK)
        
class GetMerolaganiNews(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,format=None):
        #get_merolagani_news()
        #get_sharesansar_news(1)
        #https://merolagani.com/handlers/webrequesthandler.ashx?type=get_news&newsID=0&newsCategoryID=0&symbol=&page=1&pageSize=1000&popular=false&includeFeatured=true&news=%23ctl00_ContentPlaceHolder1_txtNews&languageType=NP
        return Response({'msg':'success','merolagani':json.loads(json.dumps(get_merolagani_news()))})

class GetShareSansarNews(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,page,format=None):
        #get_merolagani_news()
        #get_sharesansar_news(1)
        #https://merolagani.com/handlers/webrequesthandler.ashx?type=get_news&newsID=0&newsCategoryID=0&symbol=&page=1&pageSize=1000&popular=false&includeFeatured=true&news=%23ctl00_ContentPlaceHolder1_txtNews&languageType=NP
        return Response({'msg':'success','sharesansar':json.loads(json.dumps(get_sharesansar_news(page)))},status=status.HTTP_200_OK)

class GetNepaliPaisaNews(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,offset,format=None):
        #get_merolagani_news()
        #get_sharesansar_news(1)
        #https://merolagani.com/handlers/webrequesthandler.ashx?type=get_news&newsID=0&newsCategoryID=0&symbol=&page=1&pageSize=1000&popular=false&includeFeatured=true&news=%23ctl00_ContentPlaceHolder1_txtNews&languageType=NP
        return Response({'msg':'success','nepalipaisa':get_nepalipaisa_news(offset)},status=status.HTTP_200_OK)

class GetBizmanduNews(APIView):
    permission_classes = (IsAuthenticated,)
    def get(self,request,offset,format=None):
        #get_merolagani_news()
        #get_sharesansar_news(1)
        #https://merolagani.com/handlers/webrequesthandler.ashx?type=get_news&newsID=0&newsCategoryID=0&symbol=&page=1&pageSize=1000&popular=false&includeFeatured=true&news=%23ctl00_ContentPlaceHolder1_txtNews&languageType=NP
        return Response({'msg':'success','nepalipaisa':getnews_bizmandu(offset)},status=status.HTTP_200_OK)