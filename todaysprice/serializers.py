from todaysprice.models import StockAndNumbers,Brokers,ListedCompanies
from rest_framework import serializers

class SymbolAndNumber(serializers.ModelSerializer):
    class Meta:
        model = StockAndNumbers
        fields = ('symbol','number')
class BrokerSerializers(serializers.ModelSerializer):
    class Meta:
        model =Brokers
        fields = ('broker_code','broker_name','landline','address')

class ListedCompanySerializers(serializers.ModelSerializer):
    class Meta:
        model =ListedCompanies
        fields = ('company_code','company_name','company_type')