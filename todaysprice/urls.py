from django.urls import path
from todaysprice import views
urlpatterns = [
    path('live/',views.TodaysPrice.as_view()),
    path('marketsummary/',views.MarketSummary.as_view()),
    path('sectorsummary/',views.SectorSummary.as_view()),
    path('topgainers/',views.TopGainers.as_view()),
    path('toploosers/',views.TopLoosers.as_view()),
    path('marketinformation/',views.MarketInformation.as_view()),
    path('marketsubindex/',views.MarketSubindex.as_view()),
    path('floorsheet/',views.MarketSubindex.as_view()),
    path('companydetails/<str:symbol>/',views.GetDetailsOfCompany.as_view()),
    path('topturnover/',views.GetTopTurnover.as_view()),
    path('topsharetraded/',views.GetTopShareTraded.as_view()),
    path('toptranscations/',views.GetTopTranscations.as_view()),

    path('getbroker/',views.GetAllBrokers.as_view()),
    path('getcompanies/',views.GetAllCompany.as_view()),
    
    path('news/merolagani/',views.GetMerolaganiNews.as_view()),
    path('news/sharesansar/<int:page>/',views.GetShareSansarNews.as_view()),
    path('news/nepalipaisa/<int:offset>/',views.GetNepaliPaisaNews.as_view()),
    path('news/bizmandu/<int:offset>/',views.GetBizmanduNews.as_view()),

]