#from LiveTrading.models import LiveTradingModels
import requests
from bs4 import BeautifulSoup
import csv
import time,json
from todaysprice.models import StockAndNumbers,Brokers,ListedCompanies


#print("S.N Name             No of Trans         Max Price           Min price       Closing price           Traded Share         Amount         Previous Closing          Differene Rs")
#f = csv.writer(open('livemarket.csv','w'))
#f.writerow(['S.N','Name','No of Transactions','Max Price','Min Price','Closing Price','Traded Share','Amount','Previous Closing Price','Difference Rs'])

#for todays share price
def scrape():
    print("inside scrape")
    pages = []
    responses = []
    #print('initial time',time.time())
    # for i in range(1,10):
    #     url = "http://www.nepalstock.com/main/todays_price/index/"+str(i)+"/"
    #     pages.append(url)
    # for item in pages:
    #print(item)
    page = requests.get("http://www.nepalstock.com/todaysprice?_limit=500")
    soup = BeautifulSoup(page.content,'html.parser')
    datas = soup.find(class_="table table-condensed table-hover")

    for tr in datas.find_all('tr')[2:181]:
        tds = tr.find_all('td')
        try:
            responses.append({'sn':tds[0].text,'name':tds[1].text,'no_of_transcations':tds[2].text,'max_price':tds[3].text,'min_price':tds[4].text,'closing_price':tds[5].text,'previous_closing_price':tds[6].text,'difference_rs':tds[7].text})
            
        except Exception as e:
             print(e)
    #print(responses)
    return responses

#ffrom sharesansar
def scrape_todaysprice_sharesansar():
    print("inside scrape")
    pages = []
    responses = []
    #print('initial time',time.time())
    # for i in range(1,10):
    #     url = "http://www.nepalstock.com/main/todays_price/index/"+str(i)+"/"
    #     pages.append(url)
    # for item in pages:
    #print(item)
    page = requests.get("https://www.sharesansar.com/today-share-price")
    soup = BeautifulSoup(page.content,'html.parser')
    datas = soup.find("table",class_="table table-bordered table-striped table-hover dataTable compact")
    #print(datas)
    for tr in datas.find_all('tr')[1:]:
        print(tr)
        tds = tr.find_all('td')
        try:
            responses.append({'sn':tds[0].text,'sym':tds[1].text.strip(),'no_of_transcations':tds[11].text,'max_price':tds[4].text,'min_price':tds[5].text,'qty':tds[8].text,'closing_price':tds[6].text,'previous_closing_price':tds[9].text,'difference_rs':tds[12].text,'difference_percent':tds[14].text})
            
        except Exception as e:
            print(e)
    print(responses)
    return responses


#for market summary
def sectorwise_summary():
    response_summary = {}
    page = requests.get("http://www.nepalstock.com")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="table table-hover table-condensed")
    #print(data)
    for tr in data.find_all('tr')[1:11]:
        tds = tr.find_all('td')
        try:
            response_summary[tds[0].text]=tds[1].text
            print(response_summary)
        except Exception as e:
            pass
    return response_summary

#top loosers
def top_loosers():
    response_loosers=[]
    page = requests.get("http://www.nepalstock.com/losers")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="dataTable table")
    dates = data.find_all('tr')[1]
    date = dates.text.strip()
    for tr in data.find_all('tr')[2:]:
        tds = tr.find_all('td')
        try:
            response_loosers.append({'date':date,'symbol':tds[0].text,'ltp':tds[1].text,'percent_change':tds[2].text})
            print(response_loosers)
        except Exception as e:
            pass
    return response_loosers

#top gainsers
def top_gainers():
    response_gainers=[]
    page = requests.get("http://www.nepalstock.com/gainers")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="dataTable table")
    dates = data.find_all('tr')[1]
    date = dates.text.strip()
    for tr in data.find_all('tr')[2:]:
        tds = tr.find_all('td')
        try:
            response_gainers.append({'date':date,'symbol':tds[0].text,'ltp':tds[1].text,'percent_change':tds[2].text})
            print(response_gainers)
        except Exception as e:
            pass
    return response_gainers

#top turnover
def top_turnover():
    response_topturnover = []
    page = requests.get("http://www.nepalstock.com/turnovers")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="dataTable table")
    tbody = data.find_all('tbody')[0]
    dates = tbody.find_all('tr')[1]
    date = dates.text.strip()
    for tr in tbody.find_all('tr')[1:12]:
        tds = tr.find_all('td')
        try:
            response_topturnover.append({'date':date,'symbol':tds[0].text,'turnOver':tds[1].text,'closingPrice':tds[2].text})
        except Exception as e:
            print(e)
    print(response_topturnover)
    return response_topturnover

#topshare traded
def top_sharetraded():
    response_sharetraded = []
    page = requests.get("http://www.nepalstock.com/turnovers")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="dataTable table")
    tbody = data.find_all('tbody')[1]
    dates = tbody.find_all('tr')[1]
    date = dates.text.strip()
    for tr in tbody.find_all('tr')[1:12]:
        tds = tr.find_all('td')
        try:
            response_sharetraded.append({'date':date,'symbol':tds[0].text,'shareTraded':tds[1].text,'closingPrice':tds[2].text})
        except Exception as e:
            print(e)
    print(response_sharetraded)
    return response_sharetraded

#top transcations traded
def top_transcations():
    response_toptranscations = []
    page = requests.get("http://www.nepalstock.com/turnovers")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="dataTable table")
    tbody = data.find_all('tbody')[2]
    dates = tbody.find_all('tr')[1]
    date = dates.text.strip()
    for tr in tbody.find_all('tr')[1:12]:
        tds = tr.find_all('td')
        try:
            response_toptranscations.append({'date':date,'symbol':tds[0].text,'no_of_transcations':tds[1].text,'closingPrice':tds[2].text})
        except Exception as e:
            print(e)
    print(response_toptranscations)
    return response_toptranscations




#for market summary
#for market summary
def market_summary():
    response_msummary = {}
    heading_market = ['TotalTurnoverRs','TotalTradedShares','TotalTransactions','TotalScripsTraded','TotalMarketCapitalizationRs','FloatedMarketCapitalizationRs']
    page = requests.get("http://www.nepalstock.com")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find_all(class_="table table-hover table-condensed")[3]
    i = 0
    #print(data)
    for tr in data.find_all('tr')[1:7]:
        tds = tr.find_all('td')
        try:
            txt = tds[0].text
            print(txt.replace(" ",""))
            #response_msummary[tds[0].text.replace(" ","")]=tds[1].text
	    #response_msummary.update({'totalTurnover
            response_msummary[heading_market[i]]=tds[1].text
            i+=1
            
        except Exception as e:
            pass
    print(response_msummary)
    return response_msummary


#for market information
def market_information():
    response_minfo = []
    page = requests.get("http://www.nepalstock.com")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find_all(class_="table table-hover table-condensed")[4]
    for tr in data.find_all('tr')[1:5]:
        tds = tr.find_all('td')
        try:
            response_minfo.append({'index':tds[0].text.strip(),'current':tds[1].text.strip(),'points_change':tds[2].text.strip(),'percent_change':tds[3].text.strip()})
        except Exception as e:
            pass
    print(response_minfo)
    return response_minfo


#for market information(subindes)
def market_subindex():
    response_subindex = []
    page = requests.get("http://www.nepalstock.com")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find_all(class_="table table-hover table-condensed")[5]
    for tr in data.find_all('tr')[1:12]:
        tds = tr.find_all('td')
        try:
            response_subindex.append({'index':tds[0].text.strip(),'current':tds[1].text.strip(),'points_change':tds[2].text.strip(),'percent_change':tds[3].text.strip()})
        except Exception as e:
            pass
    print(response_subindex)
    return response_subindex


#for floorsheet
def floorsheet():
    response_floorsheet = []
    page = requests.get("http://www.nepalstock.com/floorsheet?_limit=500")
    soup = BeautifulSoup(page.content,'html.parser')
    fordate = soup.find(class_="col-xs-2 col-md-2 col-sm-0")
    date = fordate.text.strip()
    data = soup.find(class_="table my-table")
    for tr in data.find_all('tr')[2:-3]:
        tds = tr.find_all('td')
        try:
            response_floorsheet.append({'date':date,'sn':tds[0].text,'contract_no':tds[1].text,'stock_symbol':tds[2].text,'buyer_broker':tds[2].text,'sellar_broker':tds[3].text,'quantity':tds[4].text,'rate':tds[5].text,'amount':tds[6].text})
        except Exception as e:
            pass
    print(response_floorsheet)
    return response_floorsheet


#for mobile app homepage
def appHomeScrape():
    response_homepage = {}
    page = requests.get("http://www.nepalstock.com")
    soup = BeautifulSoup(page.content,'html.parser')
    date = soup.find_all(class_="panel-heading")[2]
    response_homepage['date']=date.text
    nepse_info = soup.find(class_="pull-left banner-red banner-item")
    #print(nepse_info)
    title = nepse_info.find_all("div",class_="title")[0]
    currentIndex = nepse_info.find_all("div",class_="current-index")[0]
    pointChange = nepse_info.find_all("div",class_="point-change")[0]
    percentChange = nepse_info.find_all("div",class_="percent-change")[0]
    #print(title.text)
       # print(one)
    response_homepage.update({'title':title.text.strip(),'currentIndex':currentIndex.text.strip(),'pointChange':pointChange.text.strip(),'percentChange':percentChange.text.strip()})
    print(response_homepage)
    return response_homepage

#getting index charts
def get_chart_data(data,times):
    print(type(data),type(times))
    try:
        #page = requests.get("http://www.nepalstock.com/graphdata/{}/{}".format(str(data),str(times)))
        url = "http://www.nepalstock.com/graphdata/"+str(data)+"/"+str(times)
        page = requests.get(url)
       
    except Exception as e:
        print(e)
    return page.json()
#getting companys charts
def get_chart_company_data(data,tim):
    print(type(data),type(tim))
    try:
        #page = requests.get("http://www.nepalstock.com/graphdata/{}/{}".format(str(data),str(times)))
        url = "http://www.nepalstock.com/company/graphdata/"+str(data)+"/"+str(tim)
        page = requests.get(url)
       
    except Exception as e:
        print(e)
    return page.json()



def getStocksAndNumbers():
    ss = []
	# '''
	# Parses NEPSE companies page and gets corresponding number for every company
	# which can later be used to scrape historical data of each company.
	# Returns a dictionary with stock symbols as keys and corresponding
	# numbers as values.
	# '''

    url_path = "http://www.nepalstock.com.np/company?_limit=500" 
    symbol_dict = {}
    
    web_page = requests.get(url_path)
    soup = BeautifulSoup(web_page.content, 'html.parser')
    table = soup.find('table')
    col = 0
    r = 0
    for row in table.findAll('tr'):
        # Skip the first two rows (One is filtering, One is labels)
        r += 1
        if r <= 2: 
            continue
        for data in row.findAll('td'):
            # Skip pager
            if data.find('div', {'class': 'pager'}):
                break	
            if col == 3: #Stock symbol column
                symbol = data.text
                col += 1
            elif col == 5: #Stock link column
                if data.a:
                    # Get symbol number from the link 
                    symbol_num = data.a.get('href').split('/')[-1]
                    symbol_dict[symbol] = str(symbol_num)
                   # ss.append(symbol_dict)
                stockss = StockAndNumbers()
                stockss.symbol = symbol
                stockss.number = str(symbol_num)
                stockss.save()
                    
                col = 0
            else:
                col += 1
    return ss

#company details data
def company_details(symnumber):
    details = {}
    name = ['lastTradedPrice','changePrice','totalListedShare','paidUpValue','totalPaidUpValue','closingMarketPrice','marketCapitalization','name']
    page = requests.get("http://www.nepalstock.com/company/display/"+str(symnumber))
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="my-table table")
    com = data.find_all('tr')[0]
    namecompany = com.find_all('td')[0].text
    i=0
    for tr in data.find_all('tr')[7:15]:
        tds = tr.find_all('td')
        try:
            details[name[i]]=tds[1].text
            i+=1

        except Exception as e:
            print(e)
    details[name[7]]=namecompany
    return details

#getting financials report of the company
def get_financials_report(symnumber):
    details = []
    name = ['title','date','fileurl']
    page = requests.get("http://www.nepalstock.com/company/display/"+str(symnumber))
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find_all(class_="my-table table")[1]
    i=0
    for tr in data.find_all('tr')[2:]:
        tds = tr.find_all('td')
        try:
            details.append({'title':tds[0].text.strip(),'date':tds[2].text,'fileurl':tds[3].a.get('href')})

        except Exception as e:
            print(e)
        print(details)
    return details


#getting company profile datas
def get_company_profile_data(symnumber):
    details = {}
    name = ['image','nameAndSymbol','address','emailAddress','website']
    page = requests.get("http://www.nepalstock.com/company/display/"+str(symnumber))
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="my-table table")
    i=0
    j=1
    for tr in data.find_all('tr')[0:8]:
        tds = tr.find_all('td')
        if i==2:
            print(tds)
        try:
            if i==1:
                print(i)
                if tds[0].img:
                    details[name[0]]=tds[0].img.get('src')
                else:
                    details[name[0]]=""
                
                details[name[1]]=tds[3].text
                details[name[2]]=tds[5].text
                details[name[3]]=tds[7].text
                details[name[4]]=tds[9].text
            i+=1
            print(i)
        except Exception as e:
            print(e)
    return details

#getting single company floorsheet
def get_single_company_floorsheet(symbol):

    floorsheet_company = []
    page = requests.get("http://www.nepalstock.com/floorsheet?stock-symbol={}&_limit=500".format(str(symbol)))
    soup = BeautifulSoup(page.content,'html.parser')
    fordate = soup.find(class_="col-xs-2 col-md-2 col-sm-0")
    date = fordate.text.strip()
    data = soup.find(class_="table my-table")
    for tr in data.find_all('tr')[2:-3]:
        tds = tr.find_all('td')
        try:
            floorsheet_company.append({'date':date,'sn':tds[0].text,'contract_no':tds[1].text,'stock_symbol':tds[2].text,'buyer_broker':tds[3].text,'sellar_broker':tds[4].text,'quantity':tds[5].text,'rate':tds[6].text,'amount':tds[7].text})
        except Exception as e:
            pass
    print(floorsheet_company)
    return floorsheet_company


#get all brokers
def get_all_brokers():
    brokers_list=[]
    page = requests.get("https://merolagani.com/BrokerList.aspx")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="table table-striped table-hover")
    for tr in data.find_all('tr')[0:]:
        tds = tr.find_all('td')
        try:
            print(tds[0].a.text)
            brok = Brokers()
            brok.broker_code = tds[0].a.text
            brok.broker_name = tds[1].text
            brok.landline = tds[2].text
            brok.address = tds[3].text
            brok.save()
        except Exception as e:
            print(e)

   
    # try:
    #     #brokers_list.append({'name':data.div.text})
    # except Exception as e:
    #     print(e)
    print(brokers_list)

#getting all listed companies
def get_all_listedcomapnies():
    companies_list = []
    page = requests.get("http://www.nepalstock.com/company?_limit=500")
    soup = BeautifulSoup(page.content,'html.parser')
    data = soup.find(class_="my-table table")
    for tr in data.find_all('tr')[2:-1]:
        tds = tr.find_all('td')
        try:
            print(tds[2].text)
            company = ListedCompanies()
            company.company_code = tds[3].text
            company.company_name = tds[2].text.strip()
            company.company_type = tds[4].text
            company.save()

        except Exception as e:
            print(e)    

#get news of merolagani
def get_merolagani_news():
    news_list = []
    page = requests.get("https://merolagani.com/NewsList.aspx")
    soup = BeautifulSoup(page.content,'html.parser')
    newslist = soup.find(class_="news-list")
    #for di in newslist.find_all(class_="media-news media-news-md clearfix")[1].find(class_="media-wrap"):
    for di in newslist.find_all(class_="media-news media-news-md clearfix")[1:]:
        
        #d = di.find_all("div",class_="media-wrap")
        #print(d.a.img.get('src'))
        #print(di)
        try:
            d=di.find(class_="media-wrap")
            print(d.a.img.get('src'))
            dd = di.find(class_="media-body")
            print(dd.a.text,dd.span.text)
            news_list.append({'imageurl':d.a.img.get('src'),'title':dd.a.text,'date':dd.span.text,'newsurl':'https://www.merolagani.com'+d.a.get('href')})
        except Exception as e:
            print(e)
    print(news_list)
    return news_list
            
        # dd = di.find_all(class_="media-body")
        # print(dd.a.text)
#get sharesansar news
def get_sharesansar_news(page):
    news_lists = []
    page = requests.get("https://www.sharesansar.com/category/latest?page="+str(page))
    soup = BeautifulSoup(page.content,'html.parser')
    for news in soup.find_all(class_="featured-news-list margin-bottom-15"):
        div = news.find_all('div')
        #print(div)
        news_lists.append({'newsurl':div[0].a.get('href'),'imageurl':div[0].img.get('src'),'title':div[1].a.text.strip(),'date':div[1].span.text.strip()})
    print(news_lists)
    return news_lists


#getting nepalipaisa news
def get_nepalipaisa_news(offset):
    url ="https://nepalipaisa.com/Modules/News/webservices/NewsService.asmx/GetNewsByCategoryID"
    myobj = {"categoryID":"10","CompanyCode":"","portalID":"1","offset":offset,"limit":25,"cultureCode":"en-US","NewsDate":""}
    page = requests.post(url,json=myobj)
    #print (page.json())
    #print(page.text)
    dd = json.loads(page.text)
    return dd
    #print(json.loads(json.dumps(page.text)))
    #return page.text

#getting news from bizmandu
def getnews_bizmandu(offset):
    news_list = []
    page = requests.get("https://bizmandu.com/archive/market/"+str(offset))
    soup = BeautifulSoup(page.content,'html.parser')
    newslist = soup.find(class_="uk-margin-remove uk-list uk-list-line")
    for news in newslist.find_all('li')[0:]:
        aa = news.find('a')
        print(aa.get('href'))
        print(aa.get('title'))
        print(aa.div.p.text)
        image=""
        if aa.div.h3.img:
            image = aa.div.h3.img.get("src")
            print(image)
        news_list.append({'title':aa.get('title'),'newslink':aa.get('href'),'image':image,'date':aa.div.p.text.strip()})
    print(news_list)
    return news_list
        
        # if aa.get('href'):

        #     print(aa.get('href'))
        #     print(aa.get('title'))  
      