from django.db import models

# Create your models here.

class StockAndNumbers(models.Model):
    symbol = models.CharField(max_length=100,null=False,blank=False,unique=True)
    number = models.CharField(max_length=100,null=False,blank=False)

    def __str__(self):
        return "{}".format(self.symbol)

class Brokers(models.Model):
    broker_code = models.CharField(max_length=10,null=False,blank=False)
    broker_name = models.CharField(max_length=100,null=False,blank=False)
    landline = models.CharField(max_length=50,null=False,blank=False)
    address = models.CharField(max_length=50,null=False,blank=False)

    def __str__(self):
        return "{}".format(self.broker_name)

class ListedCompanies(models.Model):
    company_code = models.CharField(max_length=100,null=False,blank=False)
    company_name = models.CharField(max_length=100,null=False,blank=False)
    company_type = models.CharField(max_length=100,null=False,blank=False)

    def __str__(self):
        return "{}".format(self.company_name)