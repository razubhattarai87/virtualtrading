from django.db import models

# Create your models here.
class AppUser(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    userid = models.CharField(max_length=200,null=False,blank=False)
    pictureurl = models.CharField(max_length=500,null=False,blank=False)
    name = models.CharField(max_length=20,null=False,blank=False)
    email = models.CharField(max_length=20,null=True,blank=True)
    logintype = models.CharField(max_length=5,null=False,blank=False)

    def __str__(self):
        return "{}".format(self.name)
