from django.shortcuts import render
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from rest_framework.permissions import IsAuthenticated,AllowAny
from mobileapi.serializers import AppUserSerializer
from mobileapi.models import AppUser
from todaysprice.models import StockAndNumbers
from todaysprice.scrapping import get_single_company_floorsheet,get_company_profile_data,get_financials_report,get_chart_company_data,get_chart_data,appHomeScrape,scrape,floorsheet,market_subindex,market_information,market_summary,sectorwise_summary,top_loosers,top_gainers
import json

class AppHome(APIView):
    permission_classes=(AllowAny,)
    def get(self,request,format=None):
        #print(scrape())
        #print(json.loads(json.dumps(scrape())))
        appHomeScrape()
        #get_chart_data('D')
        return Response({'msg':'success','summary':json.loads(json.dumps(appHomeScrape())),'chartdata':get_chart_data('58','D'),'market_summary':json.loads(json.dumps(market_summary()))},status=status.HTTP_200_OK)

class RegisterUser(APIView):
    permission_classes = (AllowAny,)
    def post(self,request,format=None):
        serializers = AppUserSerializer(data=request.data)
        if serializers.isValid():
            serializers.save()
            return Response({'msg':'User Registered Successfull','result':serializers.data},status=status.HTTP_201_CREATED)
        return Response({'msg':'Error','results':serializers.errors},status=status.HTTP_400_BAD_REQUEST)

class UserRetrieve(APIView):
    permission_classes = (AllowAny,)
    def get_object(self,userid):
        try:
            user = AppUser.objects.get(userid=userid)
        except AppUser.DoesNotExist:
            return
    def get(self,request,userid,format=None):
        user = self.get_object(userid)
        userserializer = AppUserSerializer(user)
        return Response({'msg':'success','results':userserializer.data},status=status.HTTP_201_CREATED)

class GetChartData(APIView):
    permission_classes=(AllowAny,)
    def get(self,request,data,timetype,format=None):
        return Response({'msg':'success','chartdata':get_chart_data(data,timetype)},status=status.HTTP_201_CREATED)
        
class GetChartCompanyData(APIView):
    permission_classes=(AllowAny,)
    def get(self,request,symbol,timetype,format=None):
        num = StockAndNumbers.objects.get(symbol=symbol)       
        return Response({'msg':'success','chartdata':get_chart_company_data(num.number,timetype)},status=status.HTTP_201_CREATED)



class GetFinancialsReports(APIView):
    permission_classes=(AllowAny,)
    def get(self,request,symbol,format=None):
        num = StockAndNumbers.objects.get(symbol=symbol)  

        return Response({'msg':'success','financialreport':get_financials_report(num.number)},status=status.HTTP_201_CREATED)

class GetProfileOfCompany(APIView):
    permission_classes=(AllowAny,)
    def get(self,request,symbol,format=None):
        num = StockAndNumbers.objects.get(symbol=symbol)  

        return Response({'msg':'success','companyprofile':get_company_profile_data(num.number)},status=status.HTTP_201_CREATED)

class GetFloorsheetOfCompany(APIView):
    permission_classes=(AllowAny,)
    def get(self,request,symbol,format=None):
        return Response({'msg':'success','floorsheet_company':json.loads(json.dumps(get_single_company_floorsheet(symbol)))},status=status.HTTP_201_CREATED)

