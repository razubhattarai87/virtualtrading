from django.urls import path
from mobileapi import views

from todaysprice.views import GetBizmanduNews,GetNepaliPaisaNews,GetShareSansarNews,GetMerolaganiNews,GetAllCompany,GetAllBrokers,TodaysPrice,GetDetailsOfCompany,MarketSubindex,Floorsheet,GetTopTurnover,GetTopTranscations,GetTopShareTraded,TopLoosers,TopGainers


urlpatterns = [
    path('home',views.AppHome.as_view()),
    path('registeruser',views.RegisterUser.as_view()),
    path('get_user/<str:userid>/',views.UserRetrieve.as_view()),
    path('chartdata/<int:data>/<str:timetype>/',views.GetChartData.as_view()),
    path('live',TodaysPrice.as_view()),
    path('companydetail/<str:symbol>/',GetDetailsOfCompany.as_view()),
    path('get-chart-company/<str:symbol>/<str:timetype>/',views.GetChartCompanyData.as_view()),
    path('get-financialreport/<str:symbol>/',views.GetFinancialsReports.as_view()),
    path('get-profile-company/<str:symbol>/',views.GetProfileOfCompany.as_view()),
    path('get-floorsheet-company/<str:symbol>/',views.GetFloorsheetOfCompany.as_view()),
    path('get-marketsubindex/',MarketSubindex.as_view()),
    path('get-floorsheet/',Floorsheet.as_view()),
    path('get-topturnover/',GetTopTurnover.as_view()),
    path('get-topsharetraded/',GetTopShareTraded.as_view()),
    path('get-toptranscations/',GetTopTranscations.as_view()),

    path('get-toploosers/',TopLoosers.as_view()),

    path('get-topgainers/',TopGainers.as_view()),
    path('get-brokers/',GetAllBrokers.as_view()),
    path('get-listedcompanies/',GetAllCompany.as_view()),
    path('news/merolagani/',GetMerolaganiNews.as_view()),
    path('news/sharesansar/<int:page>/',GetShareSansarNews.as_view()),
    path('news/nepalipaisa/<int:offset>/',GetNepaliPaisaNews.as_view()),
    path('news/bizmandu/<int:offset>/',GetBizmanduNews.as_view()),
]