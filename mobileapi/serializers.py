from mobileapi.models import AppUser
from rest_framework import serializers
from rest_framework import exceptions

class AppUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = AppUser
        fields = ('userid','pictureurl','name','email','logintype')
        