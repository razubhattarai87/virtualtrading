from django.urls import path
from stock import views


urlpatterns = [
    path('register',views.UserRegister.as_view()),
    path('login',views.UserLogin.as_view()),
    path('users-info/<int:pk>/',views.UserDetails.as_view()),
    path('changepassword/<int:pk>/',views.ChangePassword.as_view()),
    
    #portfolio create
    path('portfolio/create',views.PortFolio.as_view()),
    path('portfolio/user/<int:pk>/',views.PortFolioGetUser.as_view()),
    
    path('buyshare/',views.BuyShareView.as_view()),
    path('buyshare/user/<int:pk>/',views.BuyShareDetailUser.as_view()),
    
    path('sellshare/',views.SellShareView.as_view()),
    path('sellshare/user/<int:pk>/',views.ShellShareDetailUser.as_view()),
    
    path('creditbalance/user/<int:pk>/',views.CreditBalance.as_view()),
    
]

