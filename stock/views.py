from django.shortcuts import render

# Create your views here.

from stock.models import User,Profile,Portfolio,BuyShare,SellShare,CreditsBalance
from rest_framework.response import Response
from rest_framework import status
from rest_framework.views import APIView
from rest_framework.parsers import MultiPartParser
from stock.serializers import ProfileSerializer,CreditSerializer,BuySerializer,SellSerializer,PortfolioSerializer,ChangePasswordSerializer,UserSerializer,LoginSerializer
from django.contrib.auth import login as django_login,logout as django_logout
from rest_framework.permissions import IsAuthenticated,AllowAny
from rest_framework.authtoken.models import Token

class UserRegister(APIView):

    permission_classes=(IsAuthenticated,)
    parser_classes = (MultiPartParser,)
    def post(self,request,format=None):
        serializer = UserSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'User Registered Successfull','result':serializer.data},status=status.HTTP_201_CREATED)
        return Response({'msg':'Error','results':serializer.errors},status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,format=None):
        user = User.objects.all()
        serializer = UserSerializer(user,many=True)
        return Response({'msg':'Success','results':serializer.data},status=status.HTTP_201_CREATED)


class UserLogin(APIView):
    permission_classes = (AllowAny,)
    def post(self,request,format=None):
        serializer = LoginSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data["user"]
        if not user:
            return Response({'msg':'Login Failed'},status=status.HTTP_400_BAD_REQUEST)
        django_login(request,user)
        token,_ = Token.objects.get_or_create(user=user)
        return Response({'msg':'Success','userid':user.id,'token':token.key},status=status.HTTP_200_OK)

class UserDetails(APIView):
    permission_classes = (IsAuthenticated,)
    def get_object(self,pk):
        try:
            user = User.objects.get(pk=pk)
            return user
        except User.DoesNotExist:
            return

    def get(self,request,pk,format=None):
        user = self.get_object(pk)
        userserializer = UserSerializer(user)
        credit = CreditsBalance.objects.get(user=pk)
        creditserilizers = CreditSerializer(credit)
        return Response({'msg':'success','results':userserializer.data,'credits':creditserilizers.data},status=status.HTTP_201_CREATED)
    
    parser_classes = (MultiPartParser,)
    def patch(self,request,pk,format=None):
        users = self.get_object(pk)
        serializer = UserSerializer(users,data=request.data,partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'success'},status=status.HTTP_201_CREATED)
        return Response({'msg':'failed','error':serializer.errors},status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self,request,pk,format=None):
        users = self.get_object(pk)
        users.delete()
        return Response({'msg':'Successfully Deleted'},status=status.HTTP_201_CREATED)

class ChangePassword(APIView):
        permission_classes = (IsAuthenticated,)
        def get_object(self,pk):
            try:
                user = User.objects.get(pk=pk)
                return user
            except User.DoesNotExist:
                return

        def post(self,request,pk,format=None):
            user = self.get_object(pk)
            serializer = ChangePasswordSerializer(data=request.data)
            if serializer.is_valid():
                if not user.check_password(serializer.data.get('oldPassword')):
                    return Response({'msg': 'Your Password Doesnot Match'}, 
                                    status=status.HTTP_400_BAD_REQUEST)
                
                # set_password also hashes the password that the user will get
                user.set_password(serializer.data.get('newPassword'))
                user.save()
                return Response({'msg': 'Password Change Successfull'}, status=status.HTTP_200_OK)

            return Response({'msg':'failed'},serializer.errors,
                            status=status.HTTP_400_BAD_REQUEST)

class PortFolio(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self,request,format=None):
        serializer = PortfolioSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'Portfolio Created Successfull','result':serializer.data},status=status.HTTP_201_CREATED)
        return Response({'msg':'Error','results':serializer.errors},status=status.HTTP_400_BAD_REQUEST)

class PortFolioGetUser(APIView):
    permission_classes = (IsAuthenticated,)
    # def get_object(self,pk):
    #     try:
    #         portfolio = PortFolio.objects.filter(pk=pk)
    #         return portfolio
    #     except User.DoesNotExist:
    #         return

    def get(self,request,pk,format=None):
        portfolio = Portfolio.objects.filter(user=pk)
        portserializer = PortfolioSerializer(portfolio,many=True)
        return Response({'msg':'success','results':portserializer.data},status=status.HTTP_201_CREATED)


class BuyShareView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self,request,format=None):
        serializer = BuySerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'Buy Order Placed','result':serializer.data},status=status.HTTP_201_CREATED)
        return Response({'msg':'Error','results':serializer.errors},status=status.HTTP_400_BAD_REQUEST)
class BuyShareDetailUser(APIView):
    permission_classes = (IsAuthenticated,)
    # def get_object(self,pk):
    #     try:
    #         portfolio = PortFolio.objects.filter(pk=pk)
    #         return portfolio
    #     except User.DoesNotExist:
    #         return

    def get(self,request,pk,format=None):
        buyshare = BuyShare.objects.filter(user=pk)
        buyserializer = BuySerializer(buyshare,many=True)
        return Response({'msg':'success','results':buyserializer.data},status=status.HTTP_201_CREATED)


class SellShareView(APIView):
    permission_classes = (IsAuthenticated,)

    def post(self,request,format=None):
        serializer = SellSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response({'msg':'Sell Order Placed','result':serializer.data},status=status.HTTP_201_CREATED)
        return Response({'msg':'Error','results':serializer.errors},status=status.HTTP_400_BAD_REQUEST)

    def get(self,request,format=None):
        datas = SellShare.objects.all()
        serializer = SellSerializer(datas,many=True)
        return Response({'msg':'Success','results':serializer.data},status=status.HTTP_201_CREATED)
        
class ShellShareDetailUser(APIView):
    permission_classes = (IsAuthenticated,)
    # def get_object(self,pk):
    #     try:
    #         portfolio = PortFolio.objects.filter(pk=pk)
    #         return portfolio
    #     except User.DoesNotExist:
    #         return

    def get(self,request,pk,format=None):
        sell = SellShare.objects.filter(user=pk)
        sellserializer = SellSerializer(sell,many=True)
        return Response({'msg':'success','results':sellserializer.data},status=status.HTTP_201_CREATED)


class CreditBalance(APIView):

    permission_classes = (IsAuthenticated,)
    def get(self,request,pk,format=None):
        creditbalance = CreditsBalance.objects.get(user=pk)
        creditserializer = CreditSerializer(creditbalance)
        return Response({'msg':'success','results':creditserializer.data},status=status.HTTP_200_OK)
