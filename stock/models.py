from django.db import models

# Create your models here.

from django.contrib.auth.models import AbstractUser
from django.utils.translation import ugettext_lazy as _
from django.conf import settings


class User(AbstractUser):
    username = models.CharField(max_length=100,blank=False,null=False)
    email = models.EmailField(_('email address'),unique=True)


    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['username','first_name','last_name']

    def __str__(self):
        return "{}".format(self.email)


class Profile(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL,on_delete=models.CASCADE,related_name='profile')
    phone = models.CharField(max_length=10,null=True,blank=True)
    image = models.ImageField(upload_to='profileImage/',blank=True)

    def __str__(Self):
        return "{}".format(self.user.username)


class Portfolio(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='portfolio')
    symbol = models.CharField(max_length=10,blank=False)
    no_of_stock = models.IntegerField()
    closing_price = models.IntegerField()
    value_as_closing_price = models.IntegerField()
    status = models.CharField(max_length=100,blank=True)
    
    def __str__(self):
        return "{}".format(self.symbol)

class BuyShare(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='buyshare')
    symbol = models.CharField(max_length=10,blank=False)
    no_of_stock = models.IntegerField()
    closing_price = models.IntegerField()
    value_as_closing_price = models.IntegerField()
    status = models.CharField(max_length=100,blank=True)
    
    def __str__(self):
        return "{}".format(self.symbol)

class SellShare(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='sellshare')
    symbol = models.CharField(max_length=10,blank=False)
    no_of_stock = models.IntegerField()
    closing_price = models.IntegerField()
    value_as_closing_price = models.IntegerField()
    status = models.CharField(max_length=100,blank=True)
    
    def __str__(self):
        return "{}".format(self.symbol)

class CreditsBalance(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    user = models.ForeignKey(User,on_delete=models.CASCADE,related_name='credits')
    initial_balance = models.BigIntegerField(default=1000000)
    available_balance = models.BigIntegerField(default=1000000)
    portfolio_investment = models.BigIntegerField(default=0)
    portfolio_worth = models.BigIntegerField()
    total_profit = models.BigIntegerField()

    def __str__(self):
        return "{}".format(self.total_balance)


