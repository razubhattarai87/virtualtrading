from stock.models import User,Profile,Portfolio,BuyShare,SellShare,CreditsBalance
from rest_framework import serializers
from django.contrib.auth import authenticate
from rest_framework import exceptions
from django.db.models import Sum


class ProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = Profile
        fields = ('phone','image')

class CreditSerializer(serializers.ModelSerializer):
    class Meta:
        model = CreditsBalance
        fields = ('created_at','user','initial_balance','available_balance','portfolio_investment','portfolio_worth','total_profit')


class UserSerializer(serializers.ModelSerializer):
    profile = ProfileSerializer(required=True)
    #credit = CreditSerializer(required=False)
    class Meta:
        model = User
        fields = ('email','first_name','last_name','password','profile')
        extra_kwargs = {'password':{'write_only':True}}

    def create(self, validated_data):
        print('validated_data',validated_data)
        profile_data = validated_data.pop('profile')
        password = validated_data.pop('password')
        user = User(**validated_data)
        user.set_password(password)
        user.save()

        Profile.objects.create(user=user,**profile_data)
        return user
    
    def update(self, instance, validated_data):
        profile_data = validated_data.pop('profile')
        profile = instance.profile
        
        instance.username = validated_data.get('username',instance.username)
        instance.first_name = validated_data.get('first_name',instance.first_name)
        instance.last_name = validated_data.get('last_name',instance.last_name)
        instance.save()

        profile.phone = profile_data.get('phone',profile.phone)
        profile.image = profile_data.get('image',profile.image)

        profile.save()
        return instance

###for login

class LoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()

    def validate(self,data):
        email = data.get("email","")
        password = data.get("password","")
       
        if email and password:
            user = authenticate(email=email,password=password)
            if user:
                if user.is_active:
                    data["user"] = user
                else:
                    msg = "User is deactivated."
                    raise exceptions.ValidationError(msg)
            else:
                msg = "Unable to login with given credentials"
                raise exceptions.ValidationError(msg)
        else:
            msg = "Must provide Email and Password"
            raise exceptions.ValidationError(msg)
        return data


class ChangePasswordSerializer(serializers.Serializer):

    oldPassword = serializers.CharField(required=True)
    newPassword = serializers.CharField(required=True)

    
class PortfolioSerializer(serializers.ModelSerializer):
    #user = UserSerializer(read_only=True)
    class Meta:
        model = Portfolio
        fields = ('user','symbol','no_of_stock','closing_price','value_as_closing_price','status')

class BuySerializer(serializers.ModelSerializer):
    #user = UserSerializer(read_only=True)
    class Meta:
        model = BuyShare
        fields = ('user','symbol','no_of_stock','closing_price','value_as_closing_price','status')
    
    def create(self, validated_data):
        
        try:
            #portfolios = Portfolio.objects.get(symbol=validated_data.get('symbol'),user=validated_data.get('user'))
            #Portfolios = Portfolio.objects.get(user=validated_data.get('user')).exist()
            #portfolios = Portfolio.objects.get(symbol=validated_data.get('symbol'),user=User.objects.get(pk=validated_data.get('user')))
            
            if Portfolio.objects.filter(user=validated_data.get('user')).exists():
                try:
                    portfolios = Portfolio.objects.get(symbol=validated_data.get('symbol'),user=validated_data.get('user'))
                    portfolios.no_of_stock = portfolios.no_of_stock + validated_data.get('no_of_stock')
                    portfolios.closing_price = validated_data.get('closing_price')
                    portfolios.value_as_closing_price = portfolios.no_of_stock * validated_data.get('closing_price')
                    portfolios.status = validated_data.get('status')
                    portfolios.save()
                     #saving credits 
                    credit = CreditsBalance.objects.get(user=validated_data.get('user'))
                    credit.user = validated_data.get('user')
                    credit.available_balance = credit.available_balance - validated_data.get('value_as_closing_price')
                    credit.portfolio_investment = credit.portfolio_investment + validated_data.get('value_as_closing_price')
                    portfolio_worth = Portfolio.objects.filter(user=validated_data.get('user')).aggregate(Sum('value_as_closing_price'))
                    credit.portfolio_worth = portfolio_worth.get('value_as_closing_price__sum')
                    credit.total_profit = credit.portfolio_worth - credit.portfolio_investment
                    print(portfolio_worth.get('value_as_closing_price__sum'))
                    credit.save()

                except Portfolio.DoesNotExist:
                    portfolio = Portfolio()
                    portfolio.user = validated_data.get('user')
                    portfolio.no_of_stock = validated_data.get('no_of_stock')
                    portfolio.symbol = validated_data.get('symbol')
                    portfolio.closing_price = validated_data.get('closing_price')
                    portfolio.value_as_closing_price = validated_data.get('value_as_closing_price')
                    portfolio.status = validated_data.get('status')
                    portfolio.save()
                     #saving credits 
                    credit = CreditsBalance.objects.get(user=validated_data.get('user'))
                    credit.user = validated_data.get('user')
                    credit.available_balance = credit.available_balance - validated_data.get('value_as_closing_price')
                    credit.portfolio_investment = credit.portfolio_investment + validated_data.get('value_as_closing_price')
                    portfolio_worth = Portfolio.objects.filter(user=validated_data.get('user')).aggregate(Sum('value_as_closing_price'))
                    credit.portfolio_worth = portfolio_worth.get('value_as_closing_price__sum')
                    credit.total_profit = credit.portfolio_worth - credit.portfolio_investment
                    print(portfolio_worth.get('value_as_closing_price__sum'))
                    credit.save()

            else:
                #print("doesnot exist")
                portfolio = Portfolio()
                portfolio.user = validated_data.get('user')
                portfolio.no_of_stock = validated_data.get('no_of_stock')
                portfolio.symbol = validated_data.get('symbol')
                portfolio.closing_price = validated_data.get('closing_price')
                portfolio.value_as_closing_price = validated_data.get('value_as_closing_price')
                portfolio.status = validated_data.get('status')
                portfolio.save()
                #saving credits if already exist user
                if CreditsBalance.objects.filter(user=validated_data.get('user')).exists():
                    credit = CreditsBalance.objects.get(user=validated_data.get('user'))
                    credit.available_balance = credit.available_balance - validated_data.get('value_as_closing_price')
                    credit.portfolio_investment = credit.portfolio_investment + validated_data.get('value_as_closing_price')
                    portfolio_worth = Portfolio.objects.filter(user=validated_data.get('user')).aggregate(Sum('value_as_closing_price'))
                    credit.portfolio_worth = portfolio_worth.get('value_as_closing_price__sum')
                    credit.total_profit = credit.portfolio_worth - credit.portfolio_investment
                    print(portfolio_worth.get('value_as_closing_price__sum'))
                    credit.save()
                else:
                    #if user is new
                    credit = CreditsBalance()
                    credit.user = validated_data.get('user')
                    credit.available_balance = credit.available_balance - validated_data.get('value_as_closing_price')
                    credit.portfolio_investment = credit.portfolio_investment + validated_data.get('value_as_closing_price')
                    portfolio_worth = Portfolio.objects.filter(user=validated_data.get('user')).aggregate(Sum('value_as_closing_price'))
                    credit.portfolio_worth = portfolio_worth.get('value_as_closing_price__sum')
                    credit.total_profit = credit.portfolio_worth - credit.portfolio_investment
                    print(portfolio_worth.get('value_as_closing_price__sum'))
                    credit.save()

        except Exception as e:
            print("e",e)
                
                
        
        
        buy = BuyShare(**validated_data)
        buy.save()
        return buy


        

    
    

class SellSerializer(serializers.ModelSerializer):
    #user = UserSerializer(read_only=True)
    class Meta:
        model = SellShare
        fields = ('user','symbol','no_of_stock','closing_price','value_as_closing_price','status')

    def create(self, validated_data):
    
        try:
        
            if Portfolio.objects.filter(user=validated_data.get('user')).exists():
                print("exist")
                try:
                    portfolio = Portfolio.objects.get(symbol=validated_data.get('symbol'),user=validated_data.get('user'))
                    if  portfolio.no_of_stock >= validated_data.get('no_of_stock'):
                        portfolio.no_of_stock = portfolio.no_of_stock-validated_data.get('no_of_stock')
                        portfolio.closing_price = validated_data.get('closing_price')
                        portfolio.value_as_closing_price = portfolio.no_of_stock * validated_data.get('closing_price')
                        portfolio.status = validated_data.get('status')
                        portfolio.save()
                        print("exist try")
                        #saving credits 
                        credit = CreditsBalance.objects.get(user=validated_data.get('user'))
                        credit.user = validated_data.get('user')
                        credit.available_balance = credit.available_balance + validated_data.get('value_as_closing_price')
                        credit.portfolio_investment = credit.portfolio_investment - validated_data.get('value_as_closing_price')
                        portfolio_worth = Portfolio.objects.filter(user=validated_data.get('user')).aggregate(Sum('value_as_closing_price'))
                        credit.portfolio_worth = portfolio_worth.get('value_as_closing_price__sum')
                        credit.total_profit = credit.portfolio_worth - credit.portfolio_investment
                        print(portfolio_worth.get('value_as_closing_price__sum'))
                        credit.save()

                        sell = SellShare(**validated_data)
                        sell.save()
                        return sell
                    else:
                        msg = "Not enough stock to sell"
                        raise exceptions.ValidationError(msg)
                        return msg
                except Portfolio.DoesNotExist:
                    print("doesnot exit")
                    msg = "portfolio doesnot exist"
                    raise exceptions.ValidationError(msg)
                    return msg
                    
            else:
                print("hello")
                return
                    
        except Exception as e:
            print("e",e)
            msg = e
            raise exceptions.ValidationError(msg)
            return validated_data

        #return SellShare.DoesNotExist
        
        
        
        





